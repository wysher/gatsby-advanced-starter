const DirectoryNamedWebpackPlugin = require('directory-named-webpack-plugin');
const path = require('path');

const getArray = (arr) => {
  if (!arr) return [];
  return Array.isArray(arr) ? arr : [arr];
};

const getPaths = paths => getArray(paths)
  .map(currentPath => path.resolve(currentPath));

exports.onCreateWebpackConfig = ({ actions }, pluginOptions = {}) => {
  actions.setWebpackConfig({
    resolve: {
      plugins: [
        new DirectoryNamedWebpackPlugin({
          ...pluginOptions,
          include: [
            ...getArray(pluginOptions.include),
            ...getPaths(pluginOptions.includePaths),
          ],
        }),
      ],
    },
  });
};
