import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { Link, graphql } from 'gatsby';
import {
  Hero,
  Flex,
  Box,
  Container,
  List,
  Breadcrumbs,
} from '@components';

const TagRoute = ({
  location,
  data: {
    allMarkdownRemark: {
      edges: posts,
      totalCount,
    },
    site: {
      siteMetadata,
    },
  },
  pageContext: {
    tag,
  },
}) => {
  const postLinks = posts.map(post => (
    <li key={post.node.fields.slug}>
      <Link to={post.node.fields.slug}>
        <h2 className="is-size-2">{post.node.frontmatter.title}</h2>
      </Link>
    </li>
  ));
  const tagHeader = `${totalCount} post${
    totalCount === 1 ? '' : 's'
  } tagged with “${tag}”`;

  const title = 'Tag';

  return (
    <div>
      <Helmet title={`${tag} | ${siteMetadata.title}`} />
      <Hero title={`${title}: ${tag}`} />
      <Breadcrumbs title={title} location={location} />
      <Container>
        <Flex>
          <Box>
            <h3 className="title is-size-4 is-bold-light">{tagHeader}</h3>
            <List>{postLinks}</List>
            <p>
              <Link to="/tags/">Browse all tags</Link>
            </p>
          </Box>
        </Flex>
      </Container>
    </div>
  );
};

TagRoute.propTypes = {
  data: PropTypes.shape({
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        title: PropTypes.string,
      }),
    }),
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            field: PropTypes.shape({
              slug: PropTypes.string,
            }),
            frontmatter: PropTypes.shape({
              title: PropTypes.string,
            }),
          }),
        })
      ),
    }),
  }),
  pageContext: PropTypes.shape({
    tag: PropTypes.string,
  }),
  location: PropTypes.object.isRequired,
};

export default TagRoute;

export const tagPageQuery = graphql`
  query TagPage($tag: String) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      limit: 1000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`;
