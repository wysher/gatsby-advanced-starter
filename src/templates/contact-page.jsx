import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { ContactPageTemplate } from '@components';

const ContactPage = ({ data, location }) => {
  const { frontmatter } = data.markdownRemark;
  return (
    <ContactPageTemplate
      contacts={frontmatter.contacts}
      location={location}
      metaDescription={frontmatter.metaDescription}
      metaTitle={frontmatter.metaTitle}
      subtitle={frontmatter.subtitle}
      title={frontmatter.title}
    />
  );
};

ContactPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object.isRequired,
  }),
  location: PropTypes.object.isRequired,
};

export default ContactPage;

export const contactPageQuery = graphql`
  query ContactPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        subtitle
        heading
        contacts {
          email
          description
        }
        seo {
          metaTitle
          metaDescription
        }
      }
    }
  }
`;
