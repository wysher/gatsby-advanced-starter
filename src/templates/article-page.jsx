import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { HTMLContent, ArticleTemplate } from '@components';

const ArticlePage = ({ data, location }) => {
  const { markdownRemark: post } = data;
  return (
    <ArticleTemplate
      content={post.html}
      contentComponent={HTMLContent}
      cover={post.frontmatter.cover}
      location={location}
      metaDescription={post.frontmatter.metaDescription}
      metaTitle={post.frontmatter.metaTitle}
      slug={post.fields.slug}
      tags={post.frontmatter.tags}
      title={post.frontmatter.title}
    />
  );
};

ArticlePage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object.isRequired,
  }),
  location: PropTypes.object.isRequired,
};

export default ArticlePage;

export const pageQuery = graphql`
  query ArticleByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        slug
      }
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        cover {
          src {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
          alt
        }
        tags
        seo {
          metaTitle
          metaDescription
        }
      }
    }
  }
`;
