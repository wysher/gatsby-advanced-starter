import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { HTMLContent, AboutPageTemplate } from '@components';

const AboutPage = ({ data, location }) => {
  const { markdownRemark: post } = data;
  return (
    <AboutPageTemplate
      content={post.html}
      contentComponent={HTMLContent}
      location={location}
      metaDescription={post.frontmatter.metaDescription}
      metaTitle={post.frontmatter.metaTitle}
      title={post.frontmatter.title}
    />
  );
};

AboutPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object.isRequired,
  }),
  location: PropTypes.object.isRequired,
};

export default AboutPage;

export const aboutPageQuery = graphql`
  query AboutPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
        seo {
          metaTitle
          metaDescription
        }
      }
    }
  }
`;
