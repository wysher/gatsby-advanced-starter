import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { HomePageTemplate } from '@components';

const HomePage = ({ data, location }) => {
  const { frontmatter } = data.markdownRemark;

  return (
    <HomePageTemplate
      description={frontmatter.description}
      heading={frontmatter.heading}
      location={location}
      metaDescription={frontmatter.metaDescription}
      metaTitle={frontmatter.metaTitle}
      offerings={frontmatter.offerings}
      testimonials={frontmatter.testimonials}
      title={frontmatter.title}
    />
  );
};

HomePage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object.isRequired,
  }),
  location: PropTypes.object.isRequired,
};

export default HomePage;

export const pageQuery = graphql`
  query IndexPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        heading
        description
        offerings {
          blurbs {
            image {
              src {
                id
                childImageSharp {
                  fixed {
                    ...GatsbyImageSharpFixed_tracedSVG
                  }
                }
              }
              alt
            }
            text
          }
        }
        testimonials {
          author
          quote
        }
        seo {
          metaTitle
          metaDescription
        }
      }
    }
  }
`;
