import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { PricingPageTemplate } from '@components';

const PricingPage = ({ data, location }) => {
  const { frontmatter } = data.markdownRemark;

  return (
    <PricingPageTemplate
      location={location}
      metaDescription={frontmatter.metaDescription}
      metaTitle={frontmatter.metaTitle}
      pricing={frontmatter.pricing}
      title={frontmatter.title}
    />
  );
};

PricingPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object.isRequired,
  }),
  location: PropTypes.object.isRequired,
};

export default PricingPage;

export const pricingPageQuery = graphql`
  query PricingPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        pricing {
          heading
          description
          plans {
            description
            items
            plan
            price
          }
        }
        seo {
          metaTitle
          metaDescription
        }
      }
    }
  }
`;
