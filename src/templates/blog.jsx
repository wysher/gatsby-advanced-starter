import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import config from '@data/config.json';
import {
  Breadcrumbs,
  Container,
  Hero,
  GridFlex,
  Flex,
  GridBox,
  PostCard,
  PaginationLink,
} from '@components';

const BlogPage = ({
  location,
  pageContext: {
    group,
    index,
    first,
    last,
  },
}) => {
  const previousUrl = index - 1 === 1 ? '' : (index - 1).toString();
  const nextUrl = `${(index + 1).toString()}/`;

  const websiteSchemaOrgJSONLD = {
    '@context': 'http://schema.org',
    '@type': 'WebSite',
    url: config.siteUrl,
    name: config.siteTitle,
    alternateName: config.siteTitleAlt ? config.siteTitleAlt : '',
  };

  const title = 'Blog';

  return (
    <div>
      <Helmet>
        <title>{`${title} | Gatsby Starter Business`}</title>
        {/* Schema.org tags */}
        <script type="application/ld+json">
          {JSON.stringify(websiteSchemaOrgJSONLD)}
        </script>
      </Helmet>
      <Hero title={title} />
      <Breadcrumbs title={title} location={location} />
      <Container>
        <PostCard posts={group} />
        <GridFlex>
          <GridBox width={[1, 1 / 2]}>
            <PaginationLink test={first} url={previousUrl} text="Previous Page" />
          </GridBox>
          <GridBox width={[1, 1 / 2]}>
            <Flex justifyContent="flex-end">
              <PaginationLink test={last} url={nextUrl} text="Next Page" />
            </Flex>
          </GridBox>
        </GridFlex>
      </Container>
    </div>
  );
};

BlogPage.propTypes = {
  pageContext: PropTypes.shape({
    group: PropTypes.any,
    index: PropTypes.number,
    first: PropTypes.bool,
    last: PropTypes.bool,
  }),
  location: PropTypes.object.isRequired,
};

export default BlogPage;
