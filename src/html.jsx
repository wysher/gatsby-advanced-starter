import React from 'react';
import PropTypes from 'prop-types';
import favicon from './assets/img/favicon.ico';

const HTML = ({ body, headComponents, postBodyComponents }) => (
  <html lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
      {headComponents}
      <link rel="shortcut icon" href={favicon} />
    </head>
    <body>
      <div
        id="___gatsby"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: body }}
      />
      {postBodyComponents}
    </body>
  </html>
);

HTML.propTypes = {
  headComponents: PropTypes.node,
  postBodyComponents: PropTypes.node,
  body: PropTypes.node,
};

export default HTML;
