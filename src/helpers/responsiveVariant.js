import {
  get,
  defaultBreakpoints,
  createMediaQuery,
  variant,
  cloneFunc,
  propTypes,
} from 'styled-system';

const responsiveVariant = (variantProps) => {
  const { prop = 'variant', key } = variantProps;
  const defaultVariant = variant(variantProps);

  const fn = (props) => {
    const { [prop]: variantProp } = props;

    if (!Array.isArray(variantProp)) {
      return defaultVariant;
    }

    const breakpoints = [
      null,
      ...(get(props.theme, 'breakpoints') || defaultBreakpoints).map(
        createMediaQuery
      ),
    ];

    let styles = {};

    for (let i = 0; i < variantProp.length; i++) {
      const media = breakpoints[i];
      const mediaVariant = {
        ...defaultVariant({ ...props, [prop]: variantProp[i] }),
      };

      if (!media) {
        styles = mediaVariant || {};
        continue;
      }

      const rule = mediaVariant;

      if (!rule) continue;
      styles[media] = rule;
    }

    return styles;
  };

  // fn.propTypes = { [prop]: cloneFunc(propTypes.responsive) };

  // fn.propTypes[prop].meta = {
  //   prop,
  //   themeKey: key,
  //   styleType: 'responsive',
  // };

  return fn;
};

export default responsiveVariant;
