import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { ThemeProvider } from 'styled-components';
import config from '@data/config.json';
import {
  GlobalStyles,
  Navbar,
  Footer,
  theme,
} from '@components';

class TemplateWrapper extends Component {
  state = {
    isActive: false,
  }

  toggleNavbar = (value) => {
    this.setState(prevState => ({ isActive: value !== undefined ? value : !prevState.isActive }));
  }

  render() {
    const { children } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <Fragment>
          <GlobalStyles />
          <Helmet>
            <title>{config.siteTitle}</title>
            <meta name="description" content={config.siteDescription} />
          </Helmet>
          <Navbar
            isActive={this.state.isActive}
            toggleNavbar={this.toggleNavbar}
          />
          <Fragment>{children}</Fragment>
          <Footer />
        </Fragment>
      </ThemeProvider>
    );
  }
}

TemplateWrapper.propTypes = {
  children: PropTypes.node,
};

export default TemplateWrapper;
