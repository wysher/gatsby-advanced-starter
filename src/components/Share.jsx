import React from 'react';
import PropTypes from 'prop-types';
import {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  TelegramShareButton,
  RedditShareButton,
  FacebookShareCount,
  GooglePlusShareCount,
  LinkedinShareCount,
  RedditShareCount,
  FacebookIcon,
  TwitterIcon,
  TelegramIcon,
  GooglePlusIcon,
  LinkedinIcon,
  RedditIcon,
} from 'react-share';
import config from '@data/config.json';
import { Flex } from '@components';

const Share = ({
  title,
  slug,
  excerpt,
  mobile,
}) => {
  const realPrefix = config.pathPrefix === '/' ? '' : config.pathPrefix;
  const url = config.siteUrl + realPrefix + slug;

  const iconSize = mobile ? 36 : 48;
  const filter = count => (count > 0 ? count : '');

  return (
    <Flex
      justifyContent="space-between"
      alignItems="center"
      p={[2, 3]}
    >
      <RedditShareButton url={url} title={title}>
        <RedditIcon round size={iconSize} />
        <RedditShareCount url={url}>
          {count => <div>{filter(count)}</div>}
        </RedditShareCount>
      </RedditShareButton>
      <TwitterShareButton url={url} title={title}>
        <TwitterIcon round size={iconSize} />
      </TwitterShareButton>
      <GooglePlusShareButton url={url}>
        <GooglePlusIcon round size={iconSize} />
        <GooglePlusShareCount url={url}>
          {count => <div>{filter(count)}</div>}
        </GooglePlusShareCount>
      </GooglePlusShareButton>
      <FacebookShareButton url={url} quote={excerpt}>
        <FacebookIcon round size={iconSize} />
        <FacebookShareCount url={url}>
          {count => <div>{filter(count)}</div>}
        </FacebookShareCount>
      </FacebookShareButton>
      <LinkedinShareButton
        url={url}
        title={title}
        description={excerpt}
      >
        <LinkedinIcon round size={iconSize} />
        <LinkedinShareCount url={url}>
          {count => <div>{filter(count)}</div>}
        </LinkedinShareCount>
      </LinkedinShareButton>
      <TelegramShareButton url={url}>
        <TelegramIcon round size={iconSize} />
      </TelegramShareButton>
    </Flex>
  );
};

Share.propTypes = {
  title: PropTypes.string,
  slug: PropTypes.string,
  excerpt: PropTypes.string,
  mobile: PropTypes.string,
};

export default Share;
