import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDisqusComments from 'react-disqus-comments';
import config from '@data/config.json';

class Disqus extends Component {
  state = {
    toasts: [],
  }

  onSnackbarDismiss = () => {
    const [, ...toasts] = this.state.toasts;
    this.setState({ toasts });
  }

  notifyAboutComment = () => {
    this.setState(prevState => ({
      toasts: [
        ...prevState.toasts,
        { text: 'New comment available!' },
      ],
    }));
  }

  render() {
    if (!config.disqusShortname) return null;

    const { title, slug } = this.props;
    const url = `${config.siteUrl}${config.pathPrefix}${slug}`;
    return (
      <ReactDisqusComments
        shortname={config.disqusShortname}
        identifier={title}
        title={title}
        url={url}
        onNewComment={this.notifyAboutComment}
      />
    );
  }
}

Disqus.propTypes = {
  title: PropTypes.string,
  slug: PropTypes.string,
};

export default Disqus;
