import styled from 'styled-components';
import { darken } from 'polished';
import {
  theme,
} from '@components';

export const Wrapper = styled.nav`
  background: ${theme.color.lightBackground};
  border: 1px solid ${darken(0.05, theme.color.lightBackground)};
  border-left: none;
  border-right: none;
`;

export const Item = styled.li`
  color: ${theme.color.lightText};

  &:not(:last-child):after {
    content: '|';
    margin: 0 0.5em;
  }

  &:last-child {
    a {
      pointer-events: none;
      cursor: default;
      text-decoration: none;
      color: inherit;
    }
  }
`;
