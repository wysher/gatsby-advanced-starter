import React from 'react';
import { startCase } from 'lodash';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import {
  Container,
  List,
} from '@components';
import { Wrapper, Item } from './BreadcrumbsStyles';

const getItems = ({ pathname }, title) => {
  const pathsArray = pathname.split('/').filter(Boolean);
  const { length } = pathsArray;
  const paths = pathsArray.map((val, index) => {
    const nextIndex = index + 1;

    return {
      path: nextIndex >= length ? '' : `/${pathsArray.slice(0, nextIndex).join('/')}/`,
      label: nextIndex > length ? title : startCase(val),
    };
  });
  paths.unshift({
    path: length ? '/' : '',
    label: 'Home',
  });
  return paths;
};

const Breadcrumbs = ({ title, location }) => {
  const items = getItems(location, title);
  return (
    <Wrapper>
      <Container py={2}>
        <List variant="inline">
          {items.map(({ path, label }) => (
            <Item key={label}>
              <Link to={path || '#'}>{label}</Link>
            </Item>
          ))}
        </List>
      </Container>
    </Wrapper>
  );
};

Breadcrumbs.defaultProps = {
  location: {
    pathname: '',
  },
};

Breadcrumbs.propTypes = {
  title: PropTypes.string,
  location: PropTypes.object,
};

export default Breadcrumbs;
