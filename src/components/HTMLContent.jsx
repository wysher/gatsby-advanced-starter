import React from 'react';
import PropTypes from 'prop-types';

/* eslint-disable react/no-danger */
export const HTMLContent = ({ content, className }) => (
  <div className={className} dangerouslySetInnerHTML={{ __html: content }} />
);
/* eslint-enable react/no-danger */

HTMLContent.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string,
};

export default HTMLContent;
