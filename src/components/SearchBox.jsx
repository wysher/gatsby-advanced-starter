import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { Index } from 'elasticlunr';
import { Box, Dropdown } from '@components';

class SearchBox extends Component {
  state = {
    query: '',
    results: [],
    isActive: false,
  }

  getOrCreateIndex = () => this.index
    ? this.index
    : Index.load(this.props.searchIndex);

  search = (evt) => {
    const query = evt.target.value;
    this.index = this.getOrCreateIndex();
    this.setState({
      query,
      // Query the index with search string to get an [] of IDs
      results: this.index
        .search(query, { expand: true }) // Accept partial matches
        // Map over each ID and return the full document
        .map(({ ref }) => this.index.documentStore.getDoc(ref)),
      isActive: !!query,
    });
  }

  render() {
    return (
      <Box
        mx={[0, 0, 3, 3]}
        className={`Navbar-item ${this.state.isActive ? 'is-active' : ''}`}
      >
        <input
          type="text"
          value={this.state.query}
          onChange={this.search}
          placeholder="Search"
        />
        <Dropdown>
          {this.state.results.map(page => (
            <Link key={page.id} to={page.slug}>
              <Box p={2}>
                {page.title}
              </Box>
            </Link>
          ))}
        </Dropdown>
      </Box>
    );
  }
}

SearchBox.propTypes = {
  searchIndex: PropTypes.object,
};

export default SearchBox;
