import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { Box, Panel } from '@components';

const PostCard = ({ posts }) => (
  <Box>
    {posts
      .filter(post => post.node.frontmatter.templateKey === 'article-page')
      .map(({ node: post }) => (
        <Panel p={4} mb={4} key={post.id}>
          <p>
            <Link to={post.fields.slug}>
              {post.frontmatter.title}
            </Link>
            <span> &bull; </span>
            <small>{post.frontmatter.date}</small>
          </p>
          <p>
            {post.excerpt}
            <br />
            <br />
            <Link to={post.fields.slug}>
              Keep Reading →
            </Link>
          </p>
        </Panel>
      ))}
  </Box>
);

PostCard.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      node: PropTypes.shape({
        frontmatter: PropTypes.shape({
          templateKey: PropTypes.string,
          title: PropTypes.string,
          date: PropTypes.string,
        }),
        fields: PropTypes.shape({
          slug: PropTypes.string,
        }),
        excerpt: PropTypes.string,
        id: PropTypes.string,
      }),
    })
  ),
};

export default PostCard;
