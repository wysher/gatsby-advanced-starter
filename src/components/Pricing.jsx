import React from 'react';
import PropTypes from 'prop-types';
import {
  GridFlex,
  GridBox,
  Panel,
  Flex,
} from '@components';

const Pricing = ({ data }) => (
  <GridFlex>
    {data.map(({
      plan, description, price, items,
    }) => (
      <GridBox
        key={plan}
        width={[1, 1, 1 / 3]}
      >
        <Panel>
          <Flex flexDirection="column" alignItems="center">
            <h4>
              {plan}
            </h4>
            <h2>
              $
              {price}
            </h2>
            <p>
              {description}
            </p>
            <ul>
              {items.map(item => (
                <li key={item}>
                  {item}
                </li>
              ))}
            </ul>
          </Flex>
        </Panel>
      </GridBox>
    ))}
  </GridFlex>
);

Pricing.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      plan: PropTypes.string,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      description: PropTypes.string,
      items: PropTypes.array,
    })
  ),
};

export default Pricing;
