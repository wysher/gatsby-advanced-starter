import { createGlobalStyle } from 'styled-components';
import { transparentize, lighten } from 'polished';
import theme from './theme';

const GlobalStyles = createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,500,700&subset=latin-ext');
html {
  height: 100%;
  margin: 0;
  padding: 0;
}

body {
  font-family: ${theme.font.family};
  font-weight: ${theme.font.weight};
  font-size: ${theme.font.size};
  line-height: ${theme.font.lineHeight};
  color: ${theme.color.text};
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: subpixel-antialiased;
  word-wrap: break-word;
  margin: 0;
  padding: 0;
  height: 100%;
}

a {
  transition: ${theme.transition.set('color')};
  text-decoration: none;
  &, &:visited, &:active {
    color: ${theme.color.primary};
    border-bottom: 1px dashed transparent;
  }
  &:hover {
    color: ${theme.color.primary};
    border-bottom-color: 1px solid ${transparentize(0.5, theme.color.primary)};
  }
}

table {
  border-collapse: collapse;
  &, th, td {
    border: 1px solid ${theme.color.lightBackground};
    padding: 0.5em;
  }
  tr:nth-child(2n) {
    background-color: ${lighten(0.0075, theme.color.lightBackground)};
  }
}

blockquote {
  border-radius: ${theme.radius.normal};
  background-color: ${transparentize(0.93, theme.color.primary)};
  color: ${transparentize(0.1, theme.color.primary)};
  border-left: 3px solid ${transparentize(0.3, theme.color.primary)};
  margin: 0;
  padding: 1.5em;
  position: relative;

  &:not(:last-of-type) {
    margin-bottom: 1em;
  }

  &:after {
      content: '‘‘';
      font-family: serif;
      display: block;
      font-size: 12em;
      color: ${transparentize(0.9, theme.color.primary)};
      position: absolute;
      top: 0;
      left: 0;
      z-index: -1;
      line-height: 0.8;
      letter-spacing: -15px;
  }
}

input {
  border-radius: ${theme.radius.normal};
  padding: 0.5em 1em;
  border: 1px solid ${theme.color.border};
  transition: ${theme.transition.set('border-color')};
  font: inherit;
  font-size: inherit;

  ::placeholder {
    color: ${theme.color.lightText};
  }

  &:focus {
    border-color: ${theme.color.primary};
    outline: none;
  }
}

hr {
  border-style: solid;
  border-color: ${theme.color.border};
}

p {
  margin: 1.5em 0;
}

h1, h2, h3, h4, h5, h6 {
  margin: 0;
  font-weight: 500;
}

strong {
  font-weight: 700;
}

h1 {
  font-size: ${theme.font.h1Size};
}

h2 {
  font-size: ${theme.font.h2Size};
}

h3 {
  font-size: ${theme.font.h3Size};
}

h4 {
  font-size: ${theme.font.h4Size};
}

h5 {
    font-size: ${theme.font.h5Size};
}

h6 {
    font-size: ${theme.font.h6Size};
}

::selection {
  background: ${theme.color.primary};
  color: ${theme.color.lightBackground};
}

`;

export default GlobalStyles;
