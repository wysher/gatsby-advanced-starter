import { mix } from 'polished';

const color = {
  primary: '#005792',
  secondary: '#53cde2',
  text: '#81888c',
  lightText: '#8c97a7',
  lightBackground: '#f9f7f7',
  border: '#dfe0e2',
  blue: ['#073556', '#0B5890', '#0F76BB'],
  akcent: '#ED1B24',
};

color.darkBackground = mix(0.4, color.secondary, 'black');

const transition = {
  duration: '.15s',
  easing: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
};

transition.set = (
  property,
  duration = transition.duration,
  easing = transition.easing,
  delay = '0s'
) => `${property} ${duration} ${easing} ${delay}`;

const font = {
  family: 'Open Sans, Segoe UI, Helvetica Neue, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif',
  size: '16px',
  weight: 400,
  lineHeight: 1.83,
  h1Size: '2.441rem',
  h2Size: '1.953rem',
  h3Size: '1.563rem',
  h4Size: '1.25rem',
  h5Size: '0.8rem',
  h6size: '0.64rem',
};

const radius = {
  small: '0.5em',
  normal: '1em',
};

const shadow = '0 6px 80px -15px rgba(13, 13, 76, 0.1)';

// Variants

const listVariants = {
  inline: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  block: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
};

export default {
  color,
  transition,
  font,
  radius,
  shadow,
  listVariants,
};
