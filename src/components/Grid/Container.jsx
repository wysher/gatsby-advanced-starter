import styled from 'styled-components';
import Box from './Box';

const Container = styled(Box)`
  max-width: 1440px;
  ${props => props.small && `
    max-width: 960px;
  `}
`;

Container.defaultProps = {
  mx: 'auto',
  py: [2, 3, 4],
  px: [3, 4, 5],
};

export default Container;
