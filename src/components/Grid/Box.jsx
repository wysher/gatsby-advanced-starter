import styled from 'styled-components';
import { Box } from '@rebass/grid';
import { display } from '../styled/styledFunctions';

export default styled(Box)`
  ${display};
`;
