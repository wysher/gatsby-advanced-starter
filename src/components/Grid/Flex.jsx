import styled from 'styled-components';
import { Flex } from '@rebass/grid';
import { display } from '../styled/styledFunctions';

export default styled(Flex)`
  ${display};
`;
