import React from 'react';
import Flex from './Flex';

const GridFlex = props => <Flex flexWrap="wrap" mx={[-2, -3]} {...props} />;

export default GridFlex;
