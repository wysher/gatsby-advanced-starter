import React from 'react';
import Box from './Box';

const GridBox = props => <Box px={[2, 3]} {...props} />;

export default GridBox;
