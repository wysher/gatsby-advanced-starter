import React from 'react';
import PropTypes from 'prop-types';
import {
  Img,
  GridFlex,
  GridBox,
  Flex,
} from '@components';

const Offerings = ({ gridItems }) => (
  <GridFlex my={[2, 3, 4]}>
    {gridItems.map(({
      image: {
        alt,
        src,
      },
      text,
    }) => (
      <GridBox
        key={alt || src}
        width={[1, 1 / 2]}
      >
        <Flex flexDirection="column" alignItems="center">
          <Img alt={alt} src={src} />
          <p>{text}</p>
        </Flex>
      </GridBox>
    ))}
  </GridFlex>
);

Offerings.propTypes = {
  gridItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      image: PropTypes.shape({
        id: PropTypes.string,
        alt: PropTypes.string,
        src: PropTypes.shape({
          childImageSharp: PropTypes.shape({
            fixed: PropTypes.object,
          }),
        }),
      }),
      text: PropTypes.string,
    })
  ),
};

export default Offerings;
