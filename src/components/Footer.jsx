import React from 'react';
import config from '@data/config.json';
import styled from 'styled-components';
import {
  theme,
  Container,
} from '@components';

const Wrapper = styled.footer`
  background-color: ${theme.color.lightBackground};
`;

const replaceYear = str => str.replace('{{year}}', (new Date()).getFullYear().toString());

const Footer = () => (
  <Wrapper>
    <Container py={4}>
      {replaceYear(config.copyright)}
    </Container>
  </Wrapper>
);

export default Footer;
