export { default as AboutPageTemplate } from './AboutPageTemplate';
export { default as ArticleTemplate } from './ArticleTemplate';
export { default as ContactPageTemplate } from './ContactPageTemplate';
export { default as HomePageTemplate } from './HomePageTemplate';
export { default as PricingPageTemplate } from './PricingPageTemplate';
