import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import {
  Breadcrumbs,
  Content,
  Hero,
  Container,
} from '@components';

const AboutPageTemplate = ({
  title,
  metaDescription,
  metaTitle,
  content,
  contentComponent,
  location,
}) => {
  const PageContent = contentComponent || Content;

  return (
    <div>
      <Helmet>
        <title>{metaTitle}</title>
        <meta name="description" content={metaDescription} />
      </Helmet>
      <Hero title={title} />
      <Breadcrumbs title={title} location={location} />
      <Container>
        <PageContent content={content} />
      </Container>
    </div>
  );
};

AboutPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string,
  location: PropTypes.object.isRequired,
};

export default AboutPageTemplate;
