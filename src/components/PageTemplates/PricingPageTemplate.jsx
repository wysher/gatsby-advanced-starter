import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import {
  Breadcrumbs,
  Container,
  Hero,
  Pricing,
} from '@components';

const PricingPageTemplate = ({
  title,
  metaTitle,
  metaDescription,
  pricing,
  location,
}) => (
  <div>
    <Helmet>
      <title>{metaTitle}</title>
      <meta name="description" content={metaDescription} />
    </Helmet>
    <Hero title={title} />
    <Breadcrumbs title={title} location={location} />
    <Container>
      <h2>{pricing.heading}</h2>
      <p>{pricing.description}</p>
      <Pricing data={pricing.plans} />
    </Container>
  </div>
);

PricingPageTemplate.propTypes = {
  title: PropTypes.string,
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string,
  pricing: PropTypes.shape({
    heading: PropTypes.string,
    description: PropTypes.string,
    plans: PropTypes.array,
  }),
  location: PropTypes.object.isRequired,
};

export default PricingPageTemplate;
