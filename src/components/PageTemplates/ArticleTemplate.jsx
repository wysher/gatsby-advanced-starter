import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { kebabCase } from 'lodash';
import { Link } from 'gatsby';
import {
  Breadcrumbs,
  Hero,
  Box,
  List,
  Content,
  Container,
  SEO,
  Share,
  Disqus,
  Img,
} from '@components';

const ArticleTemplate = ({
  content,
  contentComponent,
  cover,
  metaTitle,
  metaDescription,
  tags,
  title,
  slug,
  location,
}) => {
  const PostContent = contentComponent || Content;
  const { src: { childImageSharp: { fluid: { src } } } } = cover;

  const { length: tagsLength } = tags;

  return (
    <Fragment>
      <SEO
        title={title}
        metaTitle={metaTitle}
        metaDescription={metaDescription}
        cover={src}
        slug={slug}
      />
      <Hero title={`${title}`} />
      <Breadcrumbs title={title} location={location} />
      <Container small>
        <Img {...cover} />
        <PostContent content={content} />
        {tags && tagsLength ? (
          <Fragment>
            <h4>Tags</h4>
            <List variant="inline">
              {tags.map((tag, i) => {
                const isLast = i < tagsLength - 1;
                return (
                  <li key={`${tag}tag`}>
                    <Box mr={isLast ? 1 : 0}>
                      <Link to={`/tags/${kebabCase(tag)}/`}>
                        {tag}
                      </Link>
                      {isLast && ','}
                    </Box>
                  </li>
                );
              })}
            </List>
          </Fragment>
        ) : null}
        <hr />
        <Share
          title={title}
          slug={slug}
          excerpt={metaDescription}
        />
        <hr />
        <Disqus
          title={title}
          slug={slug}
        />
      </Container>
    </Fragment>
  );
};

ArticleTemplate.propTypes = {
  content: PropTypes.string.isRequired,
  contentComponent: PropTypes.func,
  cover: PropTypes.shape({
    src: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        childImageSharp: PropTypes.shape({
          fluid: PropTypes.object,
        }),
      }),
    ]),
  }),
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string,
  title: PropTypes.string,
  slug: PropTypes.string,
  tags: PropTypes.arrayOf(PropTypes.string),
  location: PropTypes.object.isRequired,
};

export default ArticleTemplate;
