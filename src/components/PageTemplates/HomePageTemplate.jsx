import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import {
  Breadcrumbs,
  Hero,
  Offerings,
  Testimonials,
  Container,
} from '@components';

const HomePageTemplate = ({
  title,
  heading,
  description,
  offerings,
  metaTitle,
  metaDescription,
  testimonials,
  location,
}) => (
  <div>
    <Helmet>
      <title>{metaTitle}</title>
      <meta name="description" content={metaDescription} />
    </Helmet>
    <Hero title={title} />
    <Breadcrumbs title={title} location={location} />
    <Container>
      <div>
        <h2>{heading}</h2>
        <p>{description}</p>
      </div>
      <Offerings gridItems={offerings.blurbs} />
      <h2>Testimonials</h2>
      <Testimonials testimonials={testimonials} />
    </Container>
  </div>
);

HomePageTemplate.propTypes = {
  title: PropTypes.string,
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string,
  heading: PropTypes.string,
  description: PropTypes.string,
  offerings: PropTypes.shape({
    blurbs: PropTypes.array,
  }),
  testimonials: PropTypes.arrayOf(
    PropTypes.shape({
      quote: PropTypes.string,
      author: PropTypes.string,
    })
  ),
  location: PropTypes.object.isRequired,
};

export default HomePageTemplate;
