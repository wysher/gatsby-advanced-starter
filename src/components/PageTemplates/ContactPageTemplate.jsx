import React, { Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import {
  Breadcrumbs,
  Container,
  Contact,
  Hero,
  GridFlex,
  GridBox,
} from '@components';

const ContactPageTemplate = ({
  title,
  subtitle,
  metaTitle,
  metaDescription,
  contacts,
  location,
}) => (
  <Fragment>
    <Helmet>
      <title>{metaTitle}</title>
      <meta name="description" content={metaDescription} />
    </Helmet>
    <Hero title={title} subtitle={subtitle} />
    <Breadcrumbs title={title} location={location} />
    <Container>
      <GridFlex>
        {contacts.map(({ email, description }) => (
          <GridBox
            key={email}
            width={[1, 1 / 2]}
            display="flex"
          >
            <Contact
              key={email}
              email={email}
              description={description}
            />
          </GridBox>
        ))}
      </GridFlex>
    </Container>
  </Fragment>
);

ContactPageTemplate.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string,
  contacts: PropTypes.arrayOf(PropTypes.shape({
    email: PropTypes.string,
    description: PropTypes.string,
  })),
  location: PropTypes.object.isRequired,
};

export default ContactPageTemplate;
