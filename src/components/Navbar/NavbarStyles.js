import styled from 'styled-components';
import { transparentize } from 'polished';
import { theme } from '@components';

export const Item = styled.li`
transition: ${theme.transition.set('background')};

  &:hover {
    background: ${transparentize(0.8, theme.color.primary)};
  }

  a {
    display: block;
    padding: 1em;
    border-bottom: 3px solid transparent;
  }

  .active {
    border-color: ${theme.color.primary};
  }
`;
