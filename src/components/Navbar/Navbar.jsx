import React from 'react';
import PropTypes from 'prop-types';
import { Link, graphql, StaticQuery } from 'gatsby';
import SearchBox from '../SearchBox';
import {
  List,
  Box,
  Container,
  Flex,
  Offcanvas,
} from '@components';
import { Item } from './NavbarStyles';

const Navbar = ({ toggleNavbar, isActive }) => (
  <StaticQuery
    query={graphql`
            query SearchIndexQuery {
                siteSearchIndex {
                    index
                }
                site {
                  siteMetadata {
                    title
                    menu {
                      label
                      path
                    }
                  }
                }
            }
        `}
    render={({ siteSearchIndex, site }) => {
      const { menu } = site.siteMetadata;

      return (
        <Container py={0}>
          <Flex
            alignItems="center"
            justifyContent="space-between"
          >
            <Box>
              <Link to="/">
                <h4>{site.siteMetadata.title}</h4>
              </Link>
            </Box>
            <Box display={['block', 'block', 'block', 'none']}>
              <Offcanvas
                isActive={isActive}
                toggle={toggleNavbar}
                items={menu}
                searchIndex={siteSearchIndex.index}
              />
            </Box>
            <Box display={['none', 'none', 'none', 'block']}>
              <List variant="inline">
                <li>
                  <SearchBox searchIndex={siteSearchIndex.index} />
                </li>
                {menu.map(({ label, path }) => (
                  <Item key={path}>
                    <Link
                      to={path}
                      activeClassName="active"
                      onClick={() => { toggleNavbar(false); }}
                    >
                      {label}
                    </Link>
                  </Item>
                ))}
              </List>
            </Box>
          </Flex>
        </Container>
      );
    }}
  />
);

Navbar.propTypes = {
  toggleNavbar: PropTypes.func.isRequired,
  isActive: PropTypes.bool,
};

export default Navbar;
