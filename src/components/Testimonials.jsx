import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { theme } from '@components';

const Blockqoute = styled.blockquote`
  ${props => props.secondary && `
      background-color: ${transparentize(0.93, theme.color.secondary)};
      color: ${transparentize(0.1, theme.color.secondary)};
      border-left-color: ${transparentize(0.3, theme.color.secondary)};

      &:after {
          color: ${transparentize(0.9, theme.color.secondary)};
      }
  `}
`;

const Testimonials = ({ testimonials }) => (
  <div>
    {testimonials.map(({ quote, author }) => (
      <Blockqoute secondary key={quote}>
        <p>{quote}</p>
        <cite>
            –
          {' '}
          {author}
        </cite>
      </Blockqoute>
    ))}
  </div>
);

Testimonials.propTypes = {
  testimonials: PropTypes.arrayOf(
    PropTypes.shape({
      quote: PropTypes.string,
      author: PropTypes.string,
    })
  ),
};

export default Testimonials;
