import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';

const PaginationLink = ({ test, text, url }) => (
  test ? (
    <span disabled>
      {text}
    </span>
  ) : (
    <Link to={url}>
      {text}
    </Link>
  )
);

PaginationLink.propTypes = {
  test: PropTypes.bool,
  text: PropTypes.string,
  url: PropTypes.string,
};

export default PaginationLink;
