import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';
import {
  Container,
  theme,
  GridFlex,
  GridBox,
} from '@components';

const gradient = {
  from: theme.color.primary,
  to: theme.color.secondary,
};

const Wrapper = styled.div`
  color: ${transparentize(0.3, 'white')};
  background: linear-gradient(to right, ${gradient.from} 0%, ${gradient.to} 100%);
  justify-content: center;
`;

const Title = styled.h1`
  ${props => !props.withSubtitle && `
    position: relative;
    top: ${theme.font.h4Size};
  `}
`;

const Subtitle = styled.h4`
    color: ${transparentize(0.5, 'white')};
    height: calc(${theme.font.h4Size} * 2);
`;

const Hero = ({ title, subtitle }) => (
  <Wrapper>
    <Container py={[2, 3, 4]}>
      <GridFlex justifyContent="center">
        <GridBox>
          <Title withSubtitle={!!subtitle}>
            {title}
          </Title>
          <Subtitle>
            {subtitle}
          </Subtitle>
        </GridBox>
      </GridFlex>
    </Container>
  </Wrapper>
);

Hero.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

export default Hero;
