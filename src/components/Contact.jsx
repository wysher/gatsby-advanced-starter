import React from 'react';
import PropTypes from 'prop-types';
import { Panel, Flex } from '@components';

const Item = props => <Flex justifyContent="center" p={2} {...props} />;

const Contact = ({ email, description }) => (
  <Panel justifyContent="center" alignItems="center">
    <Item><a href={`mailto:${email}`}>{email}</a></Item>
    <Item>{description}</Item>
  </Panel>
);

Contact.propTypes = {
  email: PropTypes.string.isRequired,
  description: PropTypes.string,
};

export default Contact;
