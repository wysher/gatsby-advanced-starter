import React from 'react';
import PropTypes from 'prop-types';
import { ArticleTemplate } from '@components';

const ArticlePreview = ({ entry, widgetFor, getAsset }) => (
  <ArticleTemplate
    content={widgetFor('body')}
    cover={{
      src: entry.getIn(['data', 'cover', 'src']),
      alt: entry.getIn(['data', 'cover', 'alt']),
    }}
    metaTitle={entry.getIn(['data', 'metaTitle'])}
    metaDescription={entry.getIn(['data', 'metaDescription'])}
    tags={entry.getIn(['data', 'tags'])}
    title={entry.getIn(['data', 'title'])}
    slug={entry.getIn(['data', 'slug'])}
  />
);

ArticlePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
  getAsset: PropTypes.func,
};

export default ArticlePreview;
