import React from 'react';
import PropTypes from 'prop-types';
import { AboutPageTemplate } from '@components';

const AboutPagePreview = ({ entry, widgetFor }) => (
  <AboutPageTemplate
    title={entry.getIn(['data', 'title'])}
    metaTitle={entry.getIn(['data', 'metaTitle'])}
    metaDescription={entry.getIn(['data', 'metaDescription'])}
    content={widgetFor('body')}
  />
);

AboutPagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
};

export default AboutPagePreview;
