import React from 'react';
import PropTypes from 'prop-types';
import { HomePageTemplate } from '@components';

const HomePagePreview = ({ entry }) => {
  const entryBlurbs = entry.getIn(['data', 'offerings', 'blurbs']);
  const blurbs = entryBlurbs ? entryBlurbs.toJS() : [];

  const entryTestimonials = entry.getIn(['data', 'testimonials']);
  const testimonials = entryTestimonials ? entryTestimonials.toJS() : [];

  return (
    <HomePageTemplate
      title={entry.getIn(['data', 'title'])}
      metaTitle={entry.getIn(['data', 'metaTitle'])}
      metaDescription={entry.getIn(['data', 'metaDescription'])}
      heading={entry.getIn(['data', 'heading'])}
      description={entry.getIn(['data', 'description'])}
      offerings={{ blurbs }}
      testimonials={testimonials}
    />
  );
};

HomePagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
};

export default HomePagePreview;
