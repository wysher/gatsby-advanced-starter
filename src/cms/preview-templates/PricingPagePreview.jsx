import React from 'react';
import PropTypes from 'prop-types';
import { PricingPageTemplate } from '@components';

const PricingPagePreivew = ({ entry }) => {
  const entryPricingPlans = entry.getIn(['data', 'pricing', 'plans']);
  const pricingPlans = entryPricingPlans ? entryPricingPlans.toJS() : [];

  return (
    <PricingPageTemplate
      title={entry.getIn(['data', 'title'])}
      metaTitle={entry.getIn(['data', 'metaTitle'])}
      metaDescription={entry.getIn(['data', 'metaDescription'])}
      pricing={{
        heading: entry.getIn(['data', 'pricing', 'heading']),
        description: entry.getIn(['data', 'pricing', 'description']),
        plans: pricingPlans,
      }}
    />
  );
};

PricingPagePreivew.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
};

export default PricingPagePreivew;
