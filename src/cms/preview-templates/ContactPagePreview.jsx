import React from 'react';
import PropTypes from 'prop-types';
import { ContactPageTemplate } from '@components';

const ContactPagePreview = ({ entry }) => {
  const entryContacts = entry.getIn(['data', 'contacts']);
  const contacts = entryContacts ? entryContacts.toJS() : [];

  return (
    <ContactPageTemplate
      title={entry.getIn(['data', 'title'])}
      subtitle={entry.getIn(['data', 'subtitle'])}
      metaTitle={entry.getIn(['data', 'metaTitle'])}
      metaDescription={entry.getIn(['data', 'metaDescription'])}
      contacts={contacts}
    />
  );
};

ContactPagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
};

export default ContactPagePreview;
