import React from 'react';
import PropTypes from 'prop-types';
import { kebabCase } from 'lodash';
import Helmet from 'react-helmet';
import { Link, graphql } from 'gatsby';
import {
  Hero,
  Breadcrumbs,
} from '@components';

const TagsPage = ({
  data: {
    allMarkdownRemark: {
      group,
    },
    site: {
      siteMetadata,
    },
  },
  location,
}) => {
  const title = 'Tags';
  return (
    <div>
      <Helmet title={`${title} | ${siteMetadata.title}`} />
      <Hero title={title} />
      <Breadcrumbs title={title} location={location} />
      <section className="section">
        <div className="container content">
          <div className="columns">
            <div
              className="column is-10 is-offset-1"
              style={{ marginBottom: '6rem' }}
            >
              <ul className="taglist">
                {group.map(({ fieldValue, totalCount }) => (
                  <li key={fieldValue}>
                    <Link to={`/tags/${kebabCase(fieldValue)}/`}>
                      {fieldValue}
                      {' '}
                      (
                      {totalCount}
                      )
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

TagsPage.propTypes = {
  data: PropTypes.shape({
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        title: PropTypes.string,
      }),
    }),
    allMarkdownRemark: PropTypes.shape({
      group: PropTypes.arrayOf(PropTypes.shape({
        fieldValue: PropTypes.string,
        totalCount: PropTypes.number,
      })),
    }),
  }),
  location: PropTypes.object.isRequired,
};

export default TagsPage;

export const tagPageQuery = graphql`
  query TagsQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(limit: 1000) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`;
